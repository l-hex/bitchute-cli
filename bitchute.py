#!/usr/bin/env python3
import argparse
import asyncio
import io
import mimetypes
import os
from pathlib import Path
import sys
from tqdm import tqdm

import appdirs
import bitchute_client as bc

USER_DATA_DIR = Path(appdirs.user_data_dir('bitchute-uploader', 'lhex'))
SESSION_ID_FILE = USER_DATA_DIR.joinpath('sessionid')

def eprint(*args, **kwargs):
  if sys.stderr.isatty():
    print(*args, file=sys.stderr, **kwargs)

class MonitoredReader(io.BufferedReader):
  def __init__(self, path):
    super().__init__(path.open('rb'))
    self.tqdm = tqdm(desc='Upload', unit='bytes', unit_scale=True, total=path.stat().st_size)

  def read(self, *args, **kwargs):
    chunk = super().read(*args, **kwargs)
    self.tqdm.update(len(chunk))
    return chunk

def make_monitored_media(path):
  p = Path(path)
  return bc.Media(p.name, mimetypes.guess_type(p.name)[0], MonitoredReader(p))

def convert_sensitivity(s):
  LEVEL = {
    'normal': bc.Sensitivity.Normal,
    'nsfw': bc.Sensitivity.NSFW,
    'nsfl': bc.Sensitivity.NSFL,
  }
  return LEVEL[s.lower()]

def parse_args(argv):
  parser = argparse.ArgumentParser(description='Interact with BitChute from the command line.')
  parser.add_argument('-u', '--username', help='BitChute account username')
  parser.add_argument('-p', '--password', help='BitChute account password')
  parser.add_argument('-r', '--resume', action='store_true',
      help='Resume from saved session ID')
  parser.add_argument('-i', '--incognito', action='store_true',
      help='Do not save session IDs on login')

  subparsers = parser.add_subparsers(dest='action', metavar='action', required=True)

  upload = subparsers.add_parser('upload', help='Upload a video.')
  upload.add_argument('video', help='Video filename')
  upload.add_argument('-t', '--title', help='Video title')
  upload.add_argument('-d', '--description', help='Video description')
  upload.add_argument('-c', '--cover', help='Cover filename')
  upload.add_argument('-l', '--publish-later', action='store_true',
      help='Leave the video unpublished after uploading.')
  upload.add_argument('-s', '--sensitivity',
      choices=list(bc.Sensitivity), default='normal', type=convert_sensitivity,
      help='Sensitivity of the video (Normal, NSFL, NSFW). Defaults to Normal.')

  video = subparsers.add_parser('video', help='View or edit a video.')
  video.add_argument('video', help='The code of URL of the target video')

  return parser.parse_args(argv)

async def login_or_resume(client, session_id, username, password, save=False):
    user = await client.get_current_user()
    if user:
      eprint('Resuming from saved session ID...')
    else:
      if session_id:
        eprint('Saved session ID has expired.')

      if not username or not password:
        error = 'Saved session ID has expred.' \
            if session_id \
            else 'No saved session ID available.'
        raise RuntimeError(error + ' You must specify a username and password ' + \
                           'with the -u and -p arguments.')

      eprint(f'Logging in as {username}...')
      await client.login(username, password)
      if save:
        with SESSION_ID_FILE.open('w') as f:
          f.write(client.get_session_id())
      user = await client.get_current_user()

    eprint(f'Logged in as {user["display_name"]} ({user["username"]}).')
    return user

async def upload(client, args):
  eprint('Uploading video...')

  video = make_monitored_media(args.video)
  cover = bc.Media.from_file(args.cover) if args.cover else None

  url = await client.upload(video, cover=cover,
      title=args.title, description=args.description,
      publish_now=not args.publish_later, sensitivity=args.sensitivity)

  eprint(f'Done! Video uploaded to:\n  {url}')

  if not sys.stdout.isatty():
    print(url)

async def main(argv):
  args = parse_args(argv)
  
  if not USER_DATA_DIR.is_dir():
    USER_DATA_DIR.mkdir(parents=True)

  session_id = None
  if args.resume and SESSION_ID_FILE.is_file():
    with SESSION_ID_FILE.open('r') as f:
      session_id = f.read()

  async with bc.Client(session_id=session_id) as client:
    if args.action == 'upload':
      await login_or_resume(client, session_id, args.username, args.password,
                            not args.incognito)
      await upload(client, args)
    elif args.action == 'video':
      if args.resume or args.username or args.password:
        await login_or_resume(client, session_id, args.username, args.password,
                              not args.incognito)
      async with client.get_video(args.video) as video:
        # TODO
        print(video.title)
        print(video.description)
        print(video.hashtags)
        print(video.category)
        print(video.sensitivity)
        print(video.discussion_allowed)
        print(video.cover_url)
        print(video.publish_date)
        print(video.channel)
        print(video.profile)
        print(video.magnet_link)
        # can do video.save(), video.unpublish(), etc. if you own it

if __name__ == '__main__':
  try:
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(sys.argv[1:]))
  except Exception as e:
    raise e
    eprint(e)
    exit(1)
