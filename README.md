# BitChute Command-Line Interface

A work in progress. Requires [bitchute-client-python](
https://gitlab.com/l-hex/bitchute-client-python).

```
usage: bitchute.py [-h] [-u USERNAME] [-p PASSWORD] [-r] [-i] action ...

Interact with BitChute from the command line.

positional arguments:
  action
    upload              Upload a video.
    video               View or edit a video.

optional arguments:
  -h, --help            show this help message and exit
  -u USERNAME, --username USERNAME
                        BitChute account username
  -p PASSWORD, --password PASSWORD
                        BitChute account password
  -r, --resume          Resume from saved session ID
  -i, --incognito       Do not save session IDs on login
```
